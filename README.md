# RX8-Arduino

![Release](https://img.shields.io/badge/Release-1.0-blue?labelColor=grey&style=flat)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)


Control an RX-8 instrument cluster with Forza Horizon 5 or Dirt Rally 2.0 through an Arduino

## Overview

RX-Arduino captures streaming UDP data from Forza Horizon 5 or Dirt Rally 2.0 and transforms it to Mazda RX-8 instrument compatible [CAN bus](https://en.wikipedia.org/wiki/CAN_bus) messages to control the speedometer and tachometer in real time.

<img src="mazda-rx8-cluster.jpg" width="390" height="205" />

# Getting Started

The following is a guide on how to install and configure the RX-8 cluster with an Arduino.

## Requirements

**Hardware**

- [Mazda RX-8 cluster](mazda-rx8-cluster.jpg) (we found ours at a car wrecker depot!)
- [Arduino Uno microcontroller](https://store-usa.arduino.cc/products/arduino-uno-rev3)
- [CAN-BUS shield](https://www.seeedstudio.com/can-bus-shield-v2.html)
- [Ethernet shield](https://store-usa.arduino.cc/products/arduino-ethernet-shield-2)

**Software**

- [Seeed Arduino CAN Library](https://github.com/Seeed-Studio/Seeed_Arduino_CAN) (see below)
- [Python 3.7+](https://www.python.org/downloads/) (*optional*)

**Games**

- [Forza Horizon 5](https://store.steampowered.com/app/1551360/Forza_Horizon_5/)
- [Dirt Rally 2.0](https://store.steampowered.com/app/690790/DiRT_Rally_20/)

## Install Dependencies

The `mcp2515_can.h` library may be found in the Arduino IDE library manager under the name **CAN_BUS_Shield** by Seeed Studio. The version used in this application is `2.3.3`

## Forza Horizon 5

To get FH5 setup and running have a look at this [README](/src/forza/README.md)

## Dirt Rally 2.0

To get DR2 setup and running have a look at this [README](/src/dirt/README.md)

## Testing

Exercise the RX-8 cluster dials without the need for running Forza nor an Arduino (when using the server). This basic simulator acts as a game UDP client and sends a constant RPM and speed to the server (Arduino or server.py)

**Requires** [Python 3.7+](https://www.python.org/downloads/)

```
python client.py --ip <server-ip> --port <server-port>
```

If ip and port are not provided defaults of `127.0.0.1` and `5600` are used.

Additionally, to help facilitate testing of only the data specifications a basic server is provided.

The inputs and defaults are similar to the client.

```
python server.py --ip <listening-ip> --port <listening-port>
```

See help

```
python client.py --help        
usage: client.py [-h] [--ip IP address] [--port port]

Forza Horizon 5 UDP client test utility

optional arguments:
  -h, --help       show this help message and exit
  --ip IP address  The UDP server's IP address (default: 127.0.0.1)
  --port port      The UDP server's port (default: 5600)
```


## RX-8 CAN Bus Messages

Finding reliable and informative CAN Bus specifications for the RX-8 cluster is *hard*. 
The following table is an attempt at listing all of the addressable identifiers used.

Each numbered column is an addressable byte position. Byte positions marked as '--' are unknown and do not impact the cluster display.

**Warning Lights**
ID    | Description | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |
---   | ---         | ---    | ---    | ---    | ---    | ---    | ---    | ---    | ---    |
0x212 | Dynamic Stability Control (DSC) | -- | -- | -- | DSC | ABS warning/brake failure | TCS | -- | -- |
0x420 | Malfunction Indicator Lamp (MIL) | Engine temperature | Odometer | -- | -- | Oil pressure | Check engine light | Coolant, oil and battery | -- |

**Dials and Displays**
ID    | Description | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |
---   | ---         | ---    | ---    | ---    | ---    | ---    | ---    | ---    | ---    |
0x201 | Tachometer and Speedometer | RPM (high byte) | RPM (low byte) | -- | -- | Speed (high byte) | Speed (low byte) | Accelerator pedal | -- |

### Sources
 - [Driving Real Gauges](https://www.xsimulator.net/community/threads/driving-real-gauges.3278/)
 - [CAN Bus Working Code](https://www.chamberofunderstanding.co.uk/2021/06/11/rx8-project-part-21-canbus-6-working-code/)
 - [Mazda RX8 instrument cluster CAN bus control demo ](https://github.com/jimkoeh/rx8/blob/master/rx8.ino)
 - [Replacement ECU for Mazda Mark 1 RX8](https://github.com/DaveBlackH/MazdaRX8Arduino/blob/master/RX8_CANBUS.ino)
 - [Arduino Rx8 Cluster Demo](https://github.com/MarkusIppy/RX8-CLUSTER-DEMO/blob/main/RX8_Ardiuino_working_driving_sim.ino)

